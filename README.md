# Access Control

## Keycloak
Keycloak is an Open Source Identity and Access Management solution for modern Applications and Services.
This repo contains a Dockerized version of keycloak that allows RBAC to provide token-based authentication/authorization mechanisms.

## Integration 

This module is responsible for providing user management and authentication/authorization to other modules. This component relies on Keycloak[1], which is an Identity and Access Management service (IAM) used to store users, and generate and renew authentication artifacts that belong to each user. By delegating authentication and authorization to Keycloak, the applications do not need to worry about different mechanisms or how to safely store passwords. This approach also provides a higher level of security as applications do not have direct access to user credentials. They are instead provided with security tokens that give them only access to what they need.


1. The user clicks Login within the application and enters their credentials to the Web App’s page.
2. Your application forwards the user's credentials to Keycloak Authorization Server and specific to the token endpoint:
3. Keycloak validates the credentials.
4. Keycloak responds with an Access Token, Refresh Token and some more relevant information.
5. The Application can use the Access Token to call an API to access information about the user. Access Token could have plenty of claims(attributes) like the specific roles about the user (Netapp Developers, Vertical Service Providers, Experimenters) and also custom attributes that are assigned to the user like Department, Partners, Project etc.
6. The API responds with requested data based on the above Roles and custom attributes.

## System Requirements
The following ports must be exposed:

1. 8080 (for keycloak)
2. 8443 (for keycloak)

## Deploy

Install the following programs
The following must be installed.


1. docker (https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04)
2. docker-compose (https://docs.docker.com/compose/install/)

You can start Keycloak by running `docker-compose up -d` .


